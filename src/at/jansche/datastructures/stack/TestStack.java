package at.jansche.datastructures.stack;

public class TestStack {

    private Node top;

    public void add(int value) {
        Node n = new Node(value);
        if (top == null) {
            top = n;
        } else {
            n.setNext(top);
            top = n;
        }
    }

    public int get() {
        try {
            Node n = top;
            top = n.getNext();
            return n.getValue();
        } catch (NullPointerException e){
            try {
                throw new StackEmptyException("StackEmpty");
            } catch (StackEmptyException stackEmptyException) {
                stackEmptyException.printStackTrace();
            }
        }
        return 0;
    }
}
