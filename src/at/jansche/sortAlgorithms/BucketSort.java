package at.jansche.sortAlgorithms;

public class BucketSort implements Sorter {
    public static int[] sort(int[] arr, int digits) {
        int[][] sortArr = new int[10][arr.length];
        int freeSpace;
        int digit = 1;

        for (int l = 0; l < digits; l++) {
            for (int k : arr) {
                freeSpace = 0;
                for (int j = 0; j < sortArr[k / digit % 10].length; j++) {
                    if (sortArr[k / digit % 10][j] == 0) {
                        freeSpace = j;
                        break;
                    }
                }
                sortArr[k / digit % 10][freeSpace] = k;
            }
            arr = writeToArr(sortArr, arr);
            sortArr = new int[10][arr.length];
            digit = digit * 10;
        }
 /*       for (int k : arr) {
            freeSpace = 0;
            for (int j = 0; j < sortArr[k % 10].length; j++) {
                if (sortArr[k % 10][j] == 0) {
                    freeSpace = j;
                    break;
                }
            }
            sortArr[k % 10][freeSpace] = k;
        }

        //debugPrint(sortArr);
        arr = writeToArr(sortArr, arr);
        sortArr = new int[10][arr.length];

        for (int k : arr) {
            freeSpace = 0;
            for (int j = 0; j < sortArr[k / 10 % 10].length; j++) {
                if (sortArr[k / 10 % 10][j] == 0) {
                    freeSpace = j;
                    break;
                }
            }
            sortArr[k / 10 % 10][freeSpace] = k;
        }

        //debugPrint(sortArr);
        arr = writeToArr(sortArr, arr);
        sortArr = new int[10][arr.length];

        for (int k : arr) {
            freeSpace = 0;
            for (int j = 0; j < sortArr[k / 100 % 10].length; j++) {
                if (sortArr[k / 100 % 10][j] == 0) {
                    freeSpace = j;
                    break;
                }
            }
            sortArr[k / 100 % 10][freeSpace] = k;
        }

        debugPrint(sortArr);
        arr = writeToArr(sortArr, arr);*/
        return arr;
    }

    private static void debugPrint(int[][] sortArr) {
        for(int a = 0; a < 10; a++) {
            for(int b = 0; b < sortArr[0].length; b++) {
                System.out.print(sortArr[a][b]+ " - ");
            }
            System.out.println();
        }
    }

    private static int[] writeToArr(int[][] sortArr, int[] arr) {
        int freeSpace = 0;
        for(int a = 0; a < 10; a++) {
            if (freeSpace > 9) {
                break;
            }
            for(int b = 0; b < sortArr[a].length; b++) {
                if(sortArr[a][b] != 0) {
                    arr[freeSpace] = sortArr[a][b];
                    freeSpace++;
                }
            }
        }
        return arr;
    }
}
