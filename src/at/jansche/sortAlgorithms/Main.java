package at.jansche.sortAlgorithms;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int size = 5;
        int digits = 3;
        int bound = 1;
        for (int i = 0; i < digits; i++) {
            bound = bound * 10;
        }
        int[] arr = ArrayHelper.createArray(size, bound);
        printArray(arr);
        System.out.println("Wählen Sie einen Sortierarlgorithmus:");
        System.out.println("(1) Insertionsort");
        System.out.println("(2) Selectionsort");
        System.out.println("(3) Bubblesort");
        System.out.println("(4) Bucketsort");
        Scanner in = new Scanner(System.in);
        int algo = in.nextInt();
        switch (algo) {
            case 1 -> arr = InsertionSort.sort(arr);
            case 2 -> arr = SelectionSort.sort(arr);
            case 3 -> arr = BubbleSort.sort(arr);
            case 4 -> arr = BucketSort.sort(arr, digits);
            default -> System.out.println("Sie haben eine falsche Zahl eingegeben.");
        }
        printArray(arr);
    }

    public static void printArray(int[] arr) {
        for(int i : arr) {
            System.out.print(i + " - ");
        }
        System.out.println();
    }
}
