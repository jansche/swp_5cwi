package at.jansche.datastructures.queue;

public class QueueEmptyException extends Exception{
    public QueueEmptyException(String message) {
        super(message);
    }

}
