package at.jansche.sortAlgorithms;

public class InsertionSort implements Sorter {
    public static int[] sort(int[] arr) {
            for(int i = 0; i < arr.length-1; i++) {
                for(int j = i+1; j > 0; j--) {
                    if(arr[j] < arr[j-1]) {
                        int a = arr[j];
                        arr[j] = arr[j-1];
                        arr[j-1] = a;
                    }
                }
            }
        return arr;
    }
}
