package at.jansche.sortAlgorithms;

public class SelectionSort implements Sorter {
    public static int[] sort(int[] arr) {
        int a;
        int point = 0;
        int changes = 0;
        for(int i = 0; i < arr.length-1; i++) {
            a = arr[i];
            for(int j = i; j < arr.length-1; j++) {
                if (arr[j + 1] < a) {
                    a = arr[j + 1];
                    point = j+1;
                    changes++;
                }
            }
            if(changes != 0) {
                arr[point] = arr[i];
                arr[i] = a;
            }
            changes = 0;
            point = 0;
        }
        return arr;
    }
}
