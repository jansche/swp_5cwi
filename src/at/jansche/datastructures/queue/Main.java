package at.jansche.datastructures.queue;

public class Main {
    public static void main(String[] args) {
        TestQueue queue = new TestQueue();
        queue.enqueue(6);
        queue.enqueue(8);
        queue.enqueue(23);
        queue.enqueue(63);
        queue.enqueue(666);
        System.out.println(queue.dequeue());
        System.out.println(queue.dequeue());
        System.out.println(queue.dequeue());
        System.out.println(queue.dequeue());
        System.out.println(queue.dequeue());
        System.out.println(queue.dequeue());
    }
}
