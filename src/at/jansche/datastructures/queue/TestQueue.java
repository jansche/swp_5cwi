package at.jansche.datastructures.queue;

public class TestQueue {

    private Node first;

    public void enqueue(int value) {
        Node n = new Node(value);
        Node test;
        if (first == null) {
            first = n;
        } else {
            getLastNode().next = n;
        }
    }

    public int dequeue() {
        try {
            Node n = first;
            first = n.getNext();
            return n.getValue();
        } catch (NullPointerException e){
            try {
                throw new QueueEmptyException("QueueEmpty");
            } catch (QueueEmptyException queueEmptyException) {
                queueEmptyException.printStackTrace();
            }
        }
        return 0;
    }

    private Node getLastNode() {
        Node n = first;
        while (n.getNext() != null) {
            n = n.next;
        }
        return n;
    }
}
