package at.jansche.sortAlgorithms;

public class BubbleSort implements Sorter {

    public static int[] sort(int[] arr) {
        int changes;
        int loops = 1;
        do {
            changes = 0;
            for(int i = 0; i < arr.length-loops; i++) {
                if(arr[i+1] < arr[i]) {
                    int a = arr[i];
                    arr[i] = arr[i+1];
                    arr[i+1] = a;
                    changes++;
                }
            }
            loops++;
        } while(changes != 0);
        return arr;
    }
}
