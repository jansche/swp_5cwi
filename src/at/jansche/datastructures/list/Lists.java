package at.jansche.datastructures.list;

public interface Lists {
        public void add(int value);
        public int get(int index);
        public void remove(int index);
        public void clear();
}
