package at.jansche.datastructures.list;

public class TestList implements Lists {
    private Node root;

    @Override
    public void add(int value) {
        Node n = new Node(value);
        if (root == null) {
            root = n;
        } else {
            getLastNode().next = n;
        }
    }

    private Node getLastNode() {
        Node n = root;
        while (n.getNext() != null) {
            n = n.next;
        }
        return n;
    }

    @Override
    public int get(int index) {
        Node n = root;
        int i = 0;
        while (i<index){
            if (n.next==null){
                throw new IndexOutOfBoundsException("Keine Daten an dieser Stelle vorhanden");
            }
            n = n.next;
            i++;
        }
        return n.getValue();
    }

    public void printAll() {
        Node n = root;
        while (n.getNext() != null) {
            System.out.println(n.getValue());
            n = n.next;
        }
        System.out.println(n.getValue());
    }

    @Override
    public void remove(int index) {
        Node n = root;
        if(index == 0) {
            root = n.getNext();
        }
        else if(index == 1) {
            n = n.getNext();
            root.next = n.getNext();
        }
        else {
            for(int i = 0; i < index-1; i++) {
                n = n.getNext();
            }
            Node x = n.getNext();
            n.next = x.getNext();
        }
    }

    @Override
    public void clear() {
        root = null;
    }
}
