package at.jansche.sortAlgorithms;

import java.util.Random;

public class ArrayHelper {

    public static int[] createArray(int size, int bound) {

        int[] arr = new int[size];
        Random ran = new Random();
        for(int i = 0; i < arr.length; i++) {
            arr[i] = ran.nextInt(bound);
        }
        return arr;
    }
}
