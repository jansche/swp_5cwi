package at.jansche.codingcontest.bowling;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Geben Sie den Input ein:");
        String input = in.nextLine();
        //  3:1,4,6,4,7,0
        String[] inputParts = input.split(":");
        int roundsShould = Integer.parseInt(inputParts[0]);
        int roundsIs = 0;
        String[] roundParts = inputParts[1].split(",");
        int[] roundInput = new int[roundParts.length];
        Stack<Integer> roundOutput = new Stack<>();
        for(int i = 0;i < roundParts.length;i++)
        {
            roundInput[i] = Integer.parseInt(roundParts[i]);
        }



        for(int i = 0; i < roundInput.length-1; i=i+2) {
            if (roundInput[i] == 10) {
                int j = i+1;
                int v;
                try {
                    while(roundInput[j] == -1) {
                        j++;
                    }
                    v = roundInput[i] + roundInput[j];
                }   catch (ArrayIndexOutOfBoundsException e) {
                    v = roundInput[i];
                    }
                int k = j+1;
                try {
                    while(roundInput[k] == -1) {
                        k++;
                    }
                    v = v + roundInput[k];
                }   catch (ArrayIndexOutOfBoundsException e) {
                    }
                try {
                    roundOutput.add(v + roundOutput.peek());
                } catch (EmptyStackException e){
                    roundOutput.add(v);
                    }
                i--;
            }
            else if(roundInput[i] + roundInput[i+1] == 10) {
                int j = i+2;
                try {
                    while(roundInput[j] == 0) {
                        j++;
                    }
                    try {
                        roundOutput.add(roundInput[i] + roundInput[i+1] + roundInput[j] + roundOutput.peek());
                    } catch (EmptyStackException e) {
                        roundOutput.add(roundInput[i] + roundInput[i+1] + roundInput[j]);
                    }
                } catch (ArrayIndexOutOfBoundsException e) {
                    roundOutput.add(roundInput[i] + roundInput[i+1]);
                }
            }
            else if(roundInput[i] == 0 && roundInput[i+1] == 0) {
                try {
                    int u = roundOutput.peek();
                    roundOutput.add(u);
                } catch (EmptyStackException e) {
                    roundOutput.add(0);
                }
            }
            else {
                try {
                    int u = roundOutput.peek();
                    roundOutput.add(roundInput[i] + roundInput[i + 1] + u);
                } catch (EmptyStackException e) {
                    roundOutput.add(roundInput[i] + roundInput[i+1]);
                }
            }
            roundsIs++;
            if(i+2 >= roundInput.length-1 && roundsIs < roundsShould) {
                i = i-2;
            }
            if(roundsIs == roundsShould) {
                break;
            }
        }



        String output = "";
        Stack<Integer> roundOutputFinal = new Stack<>();
        int t = roundOutput.size();
        for(int i = 0; i < t; i++) {
            roundOutputFinal.add(roundOutput.pop());
        }
        for(int i = 0; i < t; i++) {
            output = output + roundOutputFinal.pop() + ",";
        }
        output = output.substring(0, output.length() - 1);
        System.out.println(output);
    }
}
